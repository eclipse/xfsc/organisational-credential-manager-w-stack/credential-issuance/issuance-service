package config

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
	redisPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/db/redis"
)

type IssuanceServiceConfig struct {
	JwksUrl  string `mapstructure:"jwksUrl" envconfig:"JWKSURL"`
	Audience string `mapstructure:"audience" envconfig:"AUDIENCE"`
	config.BaseConfig
	Redis redisPkg.Config               `mapstructure:"database"`
	Nats  cloudeventprovider.NatsConfig `envconfig:"NATS"`
}
